#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>

static const char *source_mount = "/home/vboxuser/inifolderetc/sisop/";

char biner[50];

void toBinary(char txt[]){    
    int length = strlen(txt);
    for(int i=0; i<length; i++){
        int asciiCode = txt[i];
        char temp[] = "11111111";
        for(int j=7; j>=0; j--){
            temp[j] = asciiCode%2 == 0 ? '0' : '1';
            asciiCode /= 2;
        }
        if(i == 0){
            sprintf(biner, "%s", temp);
        }
        else{
            sprintf(biner, "%s %s", biner, temp);
        }
    }
}

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};

char b_code[50];
char code[50];
char encoded_txt[1000];

void base_64(int length) {
    for (int i = 0; i < length; i++) {
        int total = 0;
        int t = 5;
        for (int j = 0; j < 6; j++) {
            int a = (int)(pow(2, t));
            total += b_code[6 * i + j] == '1' ? a : 0;
            t--;
        }
        code[i] = encoding_table[total];
    }
    code[length] = '\0';
}

void base64(char ch[], int sz) {
    for (int i = 0; i < sz; i++) {
        int asciiCode = ch[i];
        char temp[] = "11111111";
        for (int j = 7; j >= 0; j--) {
            temp[j] = asciiCode % 2 == 0 ? '0' : '1';
            asciiCode /= 2;
        }
        if (i == 0) {
            sprintf(b_code, "%s", temp);
        } else {
            sprintf(b_code, "%s%s", b_code, temp);
        }
    }
}

void encode_base64(char txt[]) {
    size_t txt_sz = strlen(txt);
    int res = txt_sz % 3;
    int r = txt_sz / 3;
    char temp[4];
    int i;
    for (i = 0; i < r; i++) {
        sprintf(temp, "%c%c%c", txt[i * 3], txt[i * 3 + 1], txt[i * 3 + 2]);
        base64(temp, 3);
        base_64(4);
        if (i == 0) {
            sprintf(encoded_txt, "%s", code);
        } else {
            sprintf(encoded_txt, "%s%s", encoded_txt, code);
        }
    }
    if (res == 1) {
        sprintf(temp, "%c", txt[txt_sz - 1]);
        base64(temp, 1);
        base_64(2);
        if (i == 0) {
            sprintf(encoded_txt, "%s==", code);
        } 
        else {
            sprintf(encoded_txt, "%s%s==", encoded_txt, code);
        }
    } else if (res == 2) {
        sprintf(temp, "%c%c", txt[txt_sz - 2], txt[txt_sz - 1]);
        base64(temp, 2);
        base_64(3);
        if (i == 0) {
            sprintf(encoded_txt, "%s=", code);
        } 
        else {
            sprintf(encoded_txt, "%s%s=", encoded_txt, code);
        }
    }
}


static  int  xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s", source_mount, path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        sprintf(fpath,"%s", source_mount);
    } else sprintf(fpath, "%s%s", source_mount, path);

    int res = 0;
    int result;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    char newname[strlen(path) + 1];
    newname[strlen(path)] = '\0';
    
    if(strlen(path) > 4){
        for (size_t i = 0; i < strlen(newname); i++) {
            newname[i] = toupper(path[i]);
        }
        result = rename(path, newname);
    }
    else{
        toBinary(path);
        result = rename(path, biner);
    }
    
    if (result == -1) {
        return -errno; 
    }

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        char n = de->d_name;
        char new[strlen(n) + 1];
        new[strlen(n)] = '\0';

        memset(&st, 0, sizeof(st));

        if (S_ISDIR(mode)) {
            if(strlen(n) > 4){
                for (size_t i = 0; i < strlen(n); i++) {
                    new[i] = toupper(n[i]);
                }
            result = rename(n, new);
            }
            else{
                toBinary(n);
                result = rename(n, biner);
            }
        }
        else{
            if(strcmp(n[0], 'L') == 0 || strcmp(n[0], 'U') == 0 || strcmp(n[0], 'T') == 0 || strcmp(n[0], 'H') == 0){
                FILE *fl_name = fopen(n, "r");
                char tmp[1000];
                while (fscanf(fl_name, "%s", tmp)){
                    encode_base64(tmp);
                    fprintf(fl_name, "%s", encoded_txt);
                }
                fclose(fl_name);
            }

            if(strlen(n) > 4){
                for (size_t i = 0; i < strlen(n); i++) {
                    new[i] = tolower(n[i]);
                }
                result = rename(n, new);
            }
            else{
                toBinary(n);
                result = rename(n, biner);
            }
        }

        if (result == -1) {
            return -errno; 
        }

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        sprintf(fpath,"%s", source_mount);
    }
    else sprintf(fpath, "%s%s", source_mount, path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){
    char password[256];
    printf("Masukkan password: ");
    scanf("%s", ps);

    if (strcmp(password, ps) != 0) {
        printf("Password salah\n");
        return -EACCES;
    }
    int fd = open(path, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open;
};

int  main(int  argc, char *argv[]){
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}